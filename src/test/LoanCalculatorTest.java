package test;

import com.zopa.loancalculator.model.Lender;
import com.zopa.loancalculator.model.Market;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LoanCalculatorTest {

    @BeforeEach
    void setUp() {
        List<Lender> lenders = new ArrayList<>();
        Lender l1 = new Lender("Fry",0.05, 100);
        Lender l2 = new Lender("Leela", 0.069, 150);
        Lender l3 = new Lender("Bender", 0.075, 800);
        Lender l4 = new Lender("Amy", 0.064, 500);
        lenders.add(l1);
        lenders.add(l2);
        lenders.add(l3);
        lenders.add(l4);
        Market market = new Market();
        market.setLenderList(lenders);
    }

    @Test
    void printCalculationDetails() {
    }
}