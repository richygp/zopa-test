package test.unit;

import com.zopa.loancalculator.calculation.SortMarketByRate;
import com.zopa.loancalculator.model.Lender;
import org.junit.jupiter.api.Test;

import static junit.framework.TestCase.assertTrue;

class SortMarketByRateTest {

    @Test
    void compare_andEquals() {
        Lender l1 = new Lender("Fry",0.05, 567);
        Lender l2 = new Lender("Leela", 0.05, 654);
        SortMarketByRate sortMarketByRate = new SortMarketByRate();
        int result = sortMarketByRate.compare(l1, l2);
        assertTrue("expected to be equal", result == 0);
    }

    @Test
    void compare_andL1BiggerThanL2() {
        Lender l1 = new Lender("Fry",0.059, 567);
        Lender l2 = new Lender("Leela", 0.05, 654);
        SortMarketByRate sortMarketByRate = new SortMarketByRate();
        int result = sortMarketByRate.compare(l1, l2);
        assertTrue("expected to be l1 bigger than l2", result > 0);
    }

    @Test
    void compare_andL1LesserThanL2() {
        Lender l1 = new Lender("Fry",0.05, 567);
        Lender l2 = new Lender("Leela", 0.07, 654);
        SortMarketByRate sortMarketByRate = new SortMarketByRate();
        int result = sortMarketByRate.compare(l1, l2);
        assertTrue("expected to be equal", result < 0);
    }
}