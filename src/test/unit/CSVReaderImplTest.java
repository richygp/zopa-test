package test.unit;

import com.zopa.loancalculator.input.CSVReaderImpl;
import com.zopa.loancalculator.input.IReader;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static junit.framework.TestCase.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CSVReaderImplTest {

    @Test()
    void read_andFileNameNull() {
        try {
            IReader reader = new CSVReaderImpl(null);
            reader.read();
            fail();
        } catch (IOException e) {
            assertEquals(e.getMessage(), "Please check the file name or file fields: null");
        }
    }

    @Test()
    void read_andFileNameDoesNotExist() {
        try {
            IReader reader = new CSVReaderImpl("/BlaTg/loÑg");
            reader.read();
            fail();
        } catch (IOException e) {
            assertEquals(e.getMessage(), "Please check the file name or file fields:" +
                    " /BlaTg/loÑg (No such file or directory)");
        }
    }
}