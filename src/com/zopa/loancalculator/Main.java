package com.zopa.loancalculator;

import com.zopa.loancalculator.calculation.LoanCalculator;
import com.zopa.loancalculator.input.CSVReaderImpl;
import com.zopa.loancalculator.input.IReader;
import com.zopa.loancalculator.model.Market;

import java.io.IOException;

public class Main {
    // Period of the loan would be set to 36 months
    private static final int PERIOD = 36;

    public static void main(String[] args) throws IOException {
        String marketFile;
        int loanAmount;
        if (args.length != 2) {
            throw new IllegalArgumentException("Please check your input: wrong number of arguments given: "
                    + args.length);
        }
        try {
            marketFile = args[0];
        } catch (NullPointerException e) {
            throw new NullPointerException("The name of the file must not be an empty string: " + e.getMessage());
        }
        try {
            loanAmount = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Second argument must be an integer: " + e.getMessage());
        }

        // Create the market and populate it from the input.
        IReader fileReader = new CSVReaderImpl(marketFile);
        Market market = new Market();
        market.setLenderList(fileReader.read());
        LoanCalculator loanCalculator = new LoanCalculator(market, PERIOD, loanAmount);
        try {
            loanCalculator.printCalculationDetails();
        } catch (IndexOutOfBoundsException e) {
            System.err.println("The market has not enough offer to satisfy your loan requirement: " + loanAmount);
        }
    }
}
