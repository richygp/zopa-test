package com.zopa.loancalculator.input;

import com.zopa.loancalculator.model.Lender;

import java.io.IOException;
import java.util.List;

/**
 * This class encapsulates the abstract logic for an input reader.
 * The target is to read an input from console. In case there is a new input format,
 * you only have to implement a new classImp to deal with that.
 */
public interface IReader {
    List<Lender> read() throws IOException;
}
