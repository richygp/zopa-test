package com.zopa.loancalculator.input;

import com.zopa.loancalculator.model.Lender;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class implements an specific reader for CSV files.
 * The name/path of the file to be read must be passed as an argument when instantiating it.
 */
public class CSVReaderImpl implements IReader {

    private static final String COMMA_SEPARATOR = ",";
    private static final String SEMICOLON_SEPARATOR = ";";

    private final String fileName;

    public CSVReaderImpl(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Reads a CSV file input and returns the list of lenders.
     *
     * @return list o Lender objects which is the available market.
     * @throws IOException if an argument is not properly described.
     *                      That could be when the file name does not exist,
     *                      or a number field is not correctly written,
     *                      or not enough fields in the CVS line.
     */
    @Override
    public List<Lender> read() throws IOException {
        String line;
        List<Lender> lenderList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(this.fileName))) {
            // Skipping first line which is the column names
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] fields = line.split(COMMA_SEPARATOR);
                Lender lender = new Lender(fields[0].trim(),
                        // Using BigDecimal instead of float or double because of precision in calculations.
                        Double.parseDouble(fields[1].trim()),
                        Integer.parseInt(fields[2].trim()));
                lenderList.add(lender);
            }
        } catch (NullPointerException | IOException | NumberFormatException | ArrayIndexOutOfBoundsException e) {
            throw new IOException("Please check the file name or file fields: " + e.getMessage(), e);
        }
        return  lenderList;
    }
}
