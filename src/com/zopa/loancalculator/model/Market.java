package com.zopa.loancalculator.model;

import com.zopa.loancalculator.calculation.SortMarketByRate;

import java.util.ArrayList;
import java.util.List;

/**
 * It will contain the list of lenders and its features.
 */
public class Market {
    private List<Lender> lenderList;

    public List<Lender> getLenderList() {
        return new ArrayList<>(lenderList);
    }

    public void setLenderList(List<Lender> lenderList) {
        this.lenderList = new ArrayList<>(lenderList);
    }

    /**
     * Sorts the lenders of the market by its rate field. The result is
     * a market list of lenders sorted.
     *
     * The specific Comparator implementation
     * is given as an argument to the List.sort static function.
     */
    public void sortByRate() {
        this.lenderList.sort(new SortMarketByRate());
    }

    @Override
    public String toString() {
        StringBuilder marketPrint = new StringBuilder();
        for(Lender lender : this.getLenderList()) {
            marketPrint.append("Lender:").append(lender.getLenderName())
                    .append(" rate:").append(lender.getRate())
                    .append(" available:").append(lender.getAvailable())
                    .append("\n");
        }
        return marketPrint.toString();
    }
}
