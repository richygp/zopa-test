package com.zopa.loancalculator.model;

/**
 * This class depicts the features of a lender.
 */
public class Lender {
    private final String lenderName;
    private final double rate;
    private final int available;

    public Lender(String lenderName, double rate, int available) {
        this.lenderName = lenderName;
        this.rate = rate;
        this.available = available;
    }

    public String getLenderName() {
        return lenderName;
    }

    public double getRate() {
        return rate;
    }

    public int getAvailable() {
        return available;
    }
}
