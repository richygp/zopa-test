package com.zopa.loancalculator.calculation;

import com.zopa.loancalculator.model.Lender;

import java.util.Comparator;

public class SortMarketByRate implements Comparator<Lender> {
    /**
     * Compares its two arguments for order.  Returns a negative integer,
     * zero, or a positive integer as the first argument is less than, equal
     * to, or greater than the second.
     *
     * @param o1 the first object to be compared.
     * @param o2 the second object to be compared.
     * @return a negative integer, zero, or a positive integer as the
     * first argument is less than, equal to, or greater than the
     * second.
     * @throws NullPointerException if an argument is null and this
     *                              comparator does not permit null arguments
     * @throws ClassCastException   if the arguments' types prevent them from
     *                              being compared by this comparator.
     */
    @Override
    public int compare(Lender o1, Lender o2) {
        return Double.compare(o1.getRate(), o2.getRate());
    }
}
