package com.zopa.loancalculator.calculation;

import com.zopa.loancalculator.model.Market;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * This class represents the needed features to a Loan Calculator
 * for being able to calculate rate, monthly and total repayment.
 */
public class LoanCalculator {
    private static final int LOAN_MINIMUM_QUANTITY = 1000;
    private static final int LOAN_MAXIMUM_QUANTITY = 15000;
    private static final int LOAN_STEP_QUANTITY = 100;
    private static final int MONTHS_IN_YEAR = 12;

    private final Market market;
    private final int period;
    private final int loanQuantity;
    private BigDecimal rate;

    public LoanCalculator(Market market, int period, int loanQuantity) {
        if(!validateLoanQuantity(loanQuantity)) {
            throw new IllegalArgumentException("Please check your input: wrong loan amount required. " +
                    "It must be among [1000, 15000] and %100: " + loanQuantity);
        }
        this.loanQuantity = loanQuantity;
        this.market = market;
        this.period = period;
    }

    /**
     * This function validates the feasibility of required loan quantity. The requirements are that
     * the desired quantity must be bigger than 1000 pounds and lesser than 15000 and multiple of 100.
     *
     * @param loanQuantity quantity of money asked for a loan.
     * @return true if the loanQuantity fulfils the requirements otherwise false.
     */
    private boolean validateLoanQuantity(int loanQuantity) {
        return loanQuantity >= LOAN_MINIMUM_QUANTITY && loanQuantity <= LOAN_MAXIMUM_QUANTITY
                && loanQuantity % LOAN_STEP_QUANTITY == 0;
    }

    /**
     * It calculates the rate of the final loan. For getting that result the function first will
     * sort the market based on the minimum rate value. After this, it will scan the amount of
     * each lender until reaching the customer desired quantity.
     * Once the function gets the index of the last needed lender, it will calculate an average
     * of interests. This will be the rate.
     * Bear in mind that since these are financial operations, it is interesting to lost the
     * minimal precision as possible. That is why the module uses BigDecimal instead of double.
     *
     * @return the final rate to be applied to the loan.
     * @throws IndexOutOfBoundsException in case that the asked quantity is not reachable. Then
     *                                      the list of lenders is fully traversed without
     *                                      reaching the loan quantity desired.
     */
    private BigDecimal calculateRate() throws IndexOutOfBoundsException {
        this.market.sortByRate();
        int loanQuantityRemain = this.loanQuantity;
        int i = 0;
        while(loanQuantityRemain > 0) {
            try {
                loanQuantityRemain -= market.getLenderList().get(i).getAvailable();
                i++;
            } catch (IndexOutOfBoundsException e) {
                throw new IndexOutOfBoundsException("Market does not have enough offer: " + e.getMessage());
            }
        }
        BigDecimal rate = BigDecimal.ZERO;
        for(int j = 0; j < i; j++) {
            rate = rate.add(new BigDecimal(market.getLenderList().get(j).getRate()));
        }
        return rate.divide(new BigDecimal(i), MathContext.DECIMAL128);
    }

    /**
     * This function calculates the discount factor to be applied.
     * Based on the formula of Annual Percentage Rate (APR): Payment = loanQuantity / DiscountFactor
     * the discount factor is ((1 + interest)^period - 1) / (interest * (1 + interest)^period).
     * This interest is the periodic interest which is the total rate divided by number of
     * months in a year (12).
     *
     * @return discountFactor to be applied for APR calculation.
     */
    private BigDecimal calculateDiscountFactor() {
        // In order to avoid double calculation of rate (discountFactor + printCalculations),
        // it will be a global feature.
        this.rate = calculateRate();
        BigDecimal periodicInterest = rate.divide(new BigDecimal(MONTHS_IN_YEAR), MathContext.DECIMAL128);
        return (periodicInterest.add(BigDecimal.ONE).pow(this.period).subtract(BigDecimal.ONE))
                .divide(periodicInterest.multiply(periodicInterest.add(BigDecimal.ONE).pow(this.period)),
                        MathContext.DECIMAL128);
    }

    /**
     * This function prints the Requested Amount, Rate, Monthly and Total payment with the required format.
     * Based on the formula of Annual Percentage Rate (APR): Monthly Payment = loanQuantity / DiscountFactor
     */
    public void printCalculationDetails() {
        BigDecimal discountFactor = calculateDiscountFactor();
        BigDecimal monthlyRepayment = new BigDecimal(this.loanQuantity).divide(discountFactor,
                MathContext.DECIMAL128);
        BigDecimal totalRepayment = monthlyRepayment.multiply(new BigDecimal(this.period));
        System.out.println("Requested Amount: £" + this.loanQuantity);
        System.out.printf("Rate: %.1f%s", this.rate.multiply(new BigDecimal(100)), "%\n");
        System.out.printf("Monthly repayment: £%.2f\n", monthlyRepayment);
        System.out.printf("Total repayment: £%.2f\n", totalRepayment);
    }
}
